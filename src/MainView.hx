package;

import haxe.ui.containers.VBox;
import haxe.ui.events.MouseEvent;

@:build(haxe.ui.ComponentBuilder.build("assets/main-view.xml"))
class MainView extends VBox {
	private var levels = {
		debug: false,
		info: false,
		warn: false,
		error: false,
		critical: false
	};

	public function new() {
		super();

		setLevels();

		ldebug.onClick = setLevels;
		linfo.onClick = setLevels;
		lwarn.onClick = setLevels;
		lerror.onClick = setLevels;
		lcritical.onClick = setLevels;

		displayInfo({
			level: 4,
			details: "big fat error blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blahblahblahblahblahblahblahblahblahblahblahblahblahblahblahblahblahblahblahbla",
			timestamp: Date.now(),
			application: "Kernel",
			number: 1
		});
	}

	private function setLevels(?v:MouseEvent) {
		levels.debug = !ldebug.value;
		levels.info = !linfo.value;
		levels.warn = !lwarn.value;
		levels.error = !lerror.value;
		levels.critical = !lcritical.value;
	}

	private function displayInfo(log:LogEntry) {
		lognumval.text = cast log.number;
		timeval.text = DateTools.format(log.timestamp, "%Y-%m-%d %H:%M:%S");
		applicationval.text = log.application;
		levelval.text = switch (log.level) {
			case 0: "Debug";
			case 1: "Info";
			case 2: "Warn";
			case 3: "Error";
			case 4: "Critical";
			default: "NaN";
		}
		detailsval.text = log.details;
		detailsval.wordWrap = true;
		detailsval.autoSize();
	}
}
