class Reader {
    public static function getLogs(app:String):Array<String> {
        final file = File.getContent('/etc/symmos/logs/$app.log');
        var logs = file.split("\n");
        logs.shift();
        return logs
    }
}
