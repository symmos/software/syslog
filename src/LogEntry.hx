typedef LogEntry = {
	level:Int,
	number:Int,
	application:String,
	timestamp:Date,
	details:String
}
